<?php

use App\Http\Controllers\Api\LeagueController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');


Route::get('/start-simulation', [LeagueController::class, 'startSimulation'])->name('league.start');
Route::post('/simulate-week/{week}', [LeagueController::class, 'simulateWeek'])->name('league.simulate');
Route::post('/week-results/{week}', [LeagueController::class, 'getWeekResults'])->name('league.results');
Route::post('/week-predictions/{week}', [LeagueController::class, 'getWeekPredictions'])->name('league.predictions');
Route::get('/tournament-table', [LeagueController::class, 'getTournamentTable'])->name('league.table');
Route::put('/update-result/{id}', [LeagueController::class, 'updateResult'])->name('league.update');
