# Champions League Simulator

### Requirements

```json
"php": "^8.2"
"laravel/framework": "^10.0"
"node": "^18.16.0"
"npm": "^9.5.1"
```  

### Installation

```bash
git clone https://github.com/ocnelias/champions-league
cd insider-champions-league

### Run with Docker

```bash
docker compose up -d
docker exec php composer install
docker exec php php artisan migrate --seed
```
