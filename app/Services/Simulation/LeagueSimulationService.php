<?php

namespace App\Services\Simulation;

use App\Models\Game;
use App\Models\Team;

class LeagueSimulationService
{
    /**
     * Simulate matches for a specific week.
     *
     * @param int $week
     * @return string
     */
    public function simulateWeek(int $week): string
    {
        $games = Game::where('week', $week)->get();

        foreach ($games as $game) {
            $homeTeam = Team::find($game->home_team_id);
            $awayTeam = Team::find($game->away_team_id);

            $homeTeamGoalProbability = $homeTeam->attack / $awayTeam->defend / 50;
            $awayTeamGoalProbability = $awayTeam->attack / $homeTeam->defend / 50;

            // Simulate goals for each team
            $homeTeamGoals = $this->simulateGoals($homeTeamGoalProbability);
            $awayTeamGoals = $this->simulateGoals($awayTeamGoalProbability);

            $game->update([
                'home_team_goals' => $homeTeamGoals,
                'away_team_goals' => $awayTeamGoals,
                'is_finished' => 1,
            ]);
        }

        return 'Week ' . $week . ' simulation completed successfully.';
    }

    /**
     * Simulate the number of goals scored based on the given probability.
     *
     * @param float $probability
     * @return int
     */
    private function simulateGoals(float $probability): int
    {
        $goals = 0;

        for ($i = 0; $i < 90; $i++) {
            if (rand(0, 100) < $probability * 100) {
                $goals++;
            }
        }

        return $goals;
    }
}
