<?php

namespace App\Services\Scoring;

use App\Models\Game;
use App\Models\Team;

class ScoringService
{
    /**
     * Get the tournament table.
     * @return array
     */
    public function getTournamentTable(): array
    {
        $teams = Team::all();
        $scoring = [];

        foreach ($teams as $team) {
            // Calculate various statistics for each team
            $totalPoints = $team->homeGames->sum(function ($game) {
                    return $game->home_team_goals > $game->away_team_goals ? 3 : ($game->home_team_goals === $game->away_team_goals ? 1 : 0);
                }) + $team->awayGames->sum(function ($game) {
                    return $game->away_team_goals > $game->home_team_goals ? 3 : ($game->away_team_goals === $game->home_team_goals ? 1 : 0);
                });

            $numberOfHomeWins = $team->homeGames->where('home_team_goals', '>', 'away_team_goals')->count();
            $numberOfHomeLosses = $team->homeGames->where('home_team_goals', '<', 'away_team_goals')->count();
            $numberOfAwayWins = $team->awayGames->where('away_team_goals', '>', 'home_team_goals')->count();
            $numberOfAwayLosses = $team->awayGames->where('away_team_goals', '<', 'home_team_goals')->count();
            $numberOfDraws = $team->homeGames->where('home_team_goals', '=', 'away_team_goals')->count()
                + $team->awayGames->where('away_team_goals', '=', 'home_team_goals')->count();

            $numberOfGames = $team->homeGames->count() + $team->awayGames->count();
            $numberOfGoalsScored = $team->homeGames->sum('home_team_goals') + $team->awayGames->sum('away_team_goals');
            $numberOfGoalsConceded = $team->homeGames->sum('away_team_goals') + $team->awayGames->sum('home_team_goals');
            $goalDifference = $numberOfGoalsScored - $numberOfGoalsConceded;

            $scoring[] = [
                'team_id' => $team->id,
                'team_name' => $team->name,
                'totalPoints' => $totalPoints,
                'numberOfHomeWins' => $numberOfHomeWins,
                'numberOfHomeLosses' => $numberOfHomeLosses,
                'numberOfAwayWins' => $numberOfAwayWins,
                'numberOfAwayLosses' => $numberOfAwayLosses,
                'numberOfDraws' => $numberOfDraws,
                'numberOfGames' => $numberOfGames,
                'numberOfGoalsScored' => $numberOfGoalsScored,
                'numberOfGoalsConceded' => $numberOfGoalsConceded,
                'goalDifference' => $goalDifference,
            ];
        }

        // Sort teams based on scoring rules
        usort($scoring, function ($a, $b) {
            if ($b['totalPoints'] !== $a['totalPoints']) {
                return $b['totalPoints'] - $a['totalPoints'];
            }
            if ($b['goalDifference'] !== $a['goalDifference']) {
                return $b['goalDifference'] - $a['goalDifference'];
            }
            return $b['numberOfGoalsScored'] - $a['numberOfGoalsScored'];
        });

        return $scoring;
    }

    /**
     * Get the results for a specific week.
     *
     * @param int $week
     * @return array
     */
    public function getWeekResults(int $week): array
    {
        $results = Game::where('week', $week)
            ->with(['homeTeam', 'awayTeam'])
            ->get()
            ->map(function ($game) {
                return [
                    'id' => $game->id,
                    'home_team_name' => $game->homeTeam->name,
                    'home_team_goals' => $game->home_team_goals,
                    'away_team_name' => $game->awayTeam->name,
                    'away_team_goals' => $game->away_team_goals,
                ];
            });

        return $results->keyBy('id')->toArray();
    }

    /**
     * Get the predictions for the teams.
     *
     * @param array $data
     * @return array
     */
    public function getWeekPredictions(array $data): array
    {
        $totalPoints = array_column($data, 'totalPoints');
        $totalPointsSum = array_sum($totalPoints);
        $predictions = [];

        foreach ($data as $team) {
            $prediction = ($team['totalPoints'] / $totalPointsSum) * 100;

            $prediction = round($prediction, 2);
            $predictions[$team['team_name']] = $prediction;
        }

        arsort($predictions);
        return $predictions;
    }
}
