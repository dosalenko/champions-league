<?php

namespace App\Services\Scheduling;

use App\Models\Team;
use App\Models\Game;
use App\DTO\GameDTO;

class GameSchedulerService
{
    private $strategy;

    public function __construct(ScheduleStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * Generate the schedule for the tournament.
     * @return void
     */
    public function generateSchedule(): void
    {
        Game::truncate();

        $teams = Team::all()->toArray();
        shuffle($teams);

        $matches = $this->strategy->generateSchedule($teams);

        foreach ($matches as $roundMatches) {
            $gameDTOs = $this->convertToDTOs($roundMatches);
            $this->storeGames($gameDTOs);
        }
    }

    /**
     * Convert matches to DTOs.
     *
     * @param array $matches
     * @return array
     */
    private function convertToDTOs(array $matches): array
    {
        $gameDTOs = [];
        foreach ($matches as $match) {
            $gameDTOs[] = new GameDTO(
                $match['home_team_id'],
                $match['away_team_id'],
                0,
                0,
                $match['week']
            );
        }
        return $gameDTOs;
    }

    /**
     * Store games in the database.
     *
     * @param array $gameDTOs
     * @return void
     */
    private function storeGames(array $gameDTOs): void
    {
        $gameData = [];
        foreach ($gameDTOs as $gameDTO) {
            $gameData[] = [
                'home_team_id' => $gameDTO->getHomeTeamId(),
                'away_team_id' => $gameDTO->getAwayTeamId(),
                'home_team_goals' => $gameDTO->getHomeTeamGoals(),
                'away_team_goals' => $gameDTO->getAwayTeamGoals(),
                'week' => $gameDTO->getWeek(),
                'season' => $gameDTO->getSeason(),
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        Game::insert($gameData);
    }
}
