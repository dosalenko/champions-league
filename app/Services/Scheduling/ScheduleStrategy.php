<?php

namespace App\Services\Scheduling;

interface ScheduleStrategy
{
    public function generateSchedule(array $teams): array;
}
