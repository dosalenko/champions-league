<?php

namespace App\Services\Scheduling;

class RoundRobinStrategy implements ScheduleStrategy
{
    /**
     * Generate the schedule for the tournament using the round-robin strategy.
     *
     * @param array $teams
     * @return array
     * @throws \InvalidArgumentException
     */
    public function generateSchedule(array $teams): array
    {
        $numTeams = count($teams);

        if ($numTeams % 2 != 0) {
            throw new \InvalidArgumentException("The number of teams must be even");
        }

        $numRounds = $numTeams - 1;
        $half = $numTeams / 2;
        $matches = [];
        $reverse = false;

        // Generate matches for each round
        for ($round = 0; $round < $numRounds * 2; $round++) {
            $roundMatches = [];
            for ($i = 0; $i < $half; $i++) {
                $homeIndex = ($round + $i) % ($numTeams - 1);
                $awayIndex = ($numTeams - 1 - $i + $round) % ($numTeams - 1);

                if ($i == 0) {
                    $awayIndex = $numTeams - 1;
                }

                $homeTeam = $teams[$homeIndex];
                $awayTeam = $teams[$awayIndex];

                if ($reverse && $round % 2 != 0) {
                    $temp = $homeTeam;
                    $homeTeam = $awayTeam;
                    $awayTeam = $temp;
                }

                $roundMatches[] = [
                    'home_team_id' => $homeTeam['id'],
                    'away_team_id' => $awayTeam['id'],
                    'week' => $round + 1,
                ];
            }
            $matches[] = $roundMatches;
            $reverse = !$reverse;
        }

        return $matches;
    }
}
