<?php

namespace App\Console\Commands;

use App\Services\Scheduling\GameSchedulerService;
use Illuminate\Console\Command;

class GenerateCalendar extends Command
{
    protected $signature = 'calendar:generate';

    protected $description = 'Generate calendar';

    protected $schedulerService;

    public function __construct(GameSchedulerService $schedulerService)
    {
        parent::__construct();
        $this->schedulerService = $schedulerService;
    }

    public function handle()
    {
        $this->schedulerService->generateSchedule();
        $this->info('Calendar generated successfully.');
    }
}
