<?php

namespace App\Console\Commands;

use App\Services\Simulation\LeagueSimulationService;
use Illuminate\Console\Command;

class SimulateWeek extends Command
{
    protected $signature = 'app:simulate {week?}';

    protected $simulationService;

    protected $description = 'Simulate a week';

    public function __construct(LeagueSimulationService $simulationService)
    {
        parent::__construct();
        $this->simulationService = $simulationService;
    }

    public function handle()
    {
        $week = $this->argument('week') ?? 1;
        $this->simulationService->simulateWeek($week);
        $this->info('Week ' . $week . ' simulated successfully.');
    }
}
