<?php

namespace App\DTO;

class GameDTO
{
    private $homeTeamId;
    private $awayTeamId;
    private $homeTeamGoals;
    private $awayTeamGoals;
    private $week;
    private $season;

    public function __construct(
        int $homeTeamId,
        int $awayTeamId,
        int $homeTeamGoals = 0,
        int $awayTeamGoals = 0,
        int $week = 1,
        string $season = ''
    )
    {
        $this->homeTeamId = $homeTeamId;
        $this->awayTeamId = $awayTeamId;
        $this->homeTeamGoals = $homeTeamGoals;
        $this->awayTeamGoals = $awayTeamGoals;
        $this->week = $week;
        $this->season = $season ?: date('Y') . '-' . (date('Y') + 1);
    }

    public function getHomeTeamId(): int
    {
        return $this->homeTeamId;
    }

    public function getAwayTeamId(): int
    {
        return $this->awayTeamId;
    }

    public function getHomeTeamGoals(): int
    {
        return $this->homeTeamGoals;
    }

    public function getAwayTeamGoals(): int
    {
        return $this->awayTeamGoals;
    }

    public function getWeek(): int
    {
        return $this->week;
    }

    public function getSeason(): string
    {
        return $this->season;
    }
}
