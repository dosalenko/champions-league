<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Scheduling\ScheduleStrategy;
use App\Services\Scheduling\RoundRobinStrategy;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(ScheduleStrategy::class, RoundRobinStrategy::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
