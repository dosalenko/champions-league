<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UpdateGameResultRequest;
use App\Models\Game;
use App\Services\Scheduling\GameSchedulerService;
use App\Services\Simulation\LeagueSimulationService;
use App\Services\Scoring\ScoringService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Console\Kernel as Artisan;

class LeagueController extends BaseController
{
    protected GameSchedulerService $schedulerService;
    protected ScoringService $scoringService;
    protected LeagueSimulationService $simulationService;
    protected Artisan $artisan;
    protected Game $game;

    public function __construct(
        GameSchedulerService $schedulerService,
        ScoringService $scoringService,
        LeagueSimulationService $simulationService,
        Artisan $artisan,
        Game $game
    ) {
        $this->schedulerService = $schedulerService;
        $this->scoringService = $scoringService;
        $this->simulationService = $simulationService;
        $this->artisan = $artisan;
        $this->game = $game;
    }

    /**
     * Start the league simulation.
     *
     * @return JsonResponse
     */
    public function startSimulation(): JsonResponse
    {
        $this->artisan->call('calendar:generate');
        $this->artisan->call('app:simulate', ['week' => 1]);

        return response()->json('success');
    }

    /**
     * Get the tournament table.
     *
     * @param Request $request
     * @param int|null $week
     * @return JsonResponse
     */
    public function getTournamentTable(Request $request, int $week = null): JsonResponse
    {
        $scoring = $this->scoringService->getTournamentTable();

        return response()->json($scoring);
    }

    /**
     * Get the results for a specific week.
     *
     * @param int $week
     * @return JsonResponse
     */
    public function getWeekResults(int $week): JsonResponse
    {
        $results = $this->scoringService->getWeekResults($week);

        return response()->json($results);
    }

    /**
     * Get the predictions for the current week.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getWeekPredictions(Request $request): JsonResponse
    {
        $results = $this->scoringService->getWeekPredictions($request->all());

        return response()->json($results);
    }

    /**
     * Simulate the matches for a specific week.
     *
     * @param int $week
     * @return JsonResponse
     */
    public function simulateWeek(int $week): JsonResponse
    {
        $this->artisan->call('app:simulate', ['week' => $week]);

        return response()->json('success');
    }

    /**
     * Update the result of a specific game.
     *
     * @param UpdateGameResultRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function updateResult(UpdateGameResultRequest $request, int $id): JsonResponse
    {
        try {
            $game = $this->game->findOrFail($id);
            $game->update([
                'home_team_goals' => $request->input('home_team_goals'),
                'away_team_goals' => $request->input('away_team_goals'),
            ]);

            return response()->json('success');
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update result'], 500);
        }
    }
}
