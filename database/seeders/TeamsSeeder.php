<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $teams = [
            ['name' => 'Liverpool', 'attack' => 90, 'defend' => 80],
            ['name' => 'Manchester City', 'attack' => 95, 'defend' => 80],
            ['name' => 'Arsenal', 'attack' => 70, 'defend' => 80],
            ['name' => 'Aston Villa', 'attack' => 60, 'defend' => 50],
        ];

        Team::insert($teams);
    }
}
