<?php

namespace Tests\Unit;

use App\Http\Controllers\Api\LeagueController;
use App\Services\Scheduling\GameSchedulerService;
use App\Services\Scoring\ScoringService;
use App\Services\Simulation\LeagueSimulationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Mockery;
use Tests\TestCase;

class LeagueTest extends TestCase
{
    public function testStartSimulation(): void
    {
        Artisan::shouldReceive('call')->once()->with('calendar:generate');
        Artisan::shouldReceive('call')->once()->with('app:simulate', ['week' => 1]);

        $controller = new LeagueController(
            Mockery::mock(GameSchedulerService::class),
            Mockery::mock(ScoringService::class),
            Mockery::mock(LeagueSimulationService::class)
        );

        $response = $controller->startSimulation();

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals('"success"', $response->getContent());
    }

    public function testGetTournamentTable(): void
    {
        $request = new Request();
        $scoringService = Mockery::mock(ScoringService::class);
        $scoringService->shouldReceive('getTournamentTable')->once()->andReturn(['table_data']);

        $controller = new LeagueController(
            Mockery::mock(GameSchedulerService::class),
            $scoringService,
            Mockery::mock(LeagueSimulationService::class)
        );

        $response = $controller->getTournamentTable($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(['table_data'], $response->getData(true));
    }

    public function testGetWeekResults(): void
    {
        $scoringService = Mockery::mock(ScoringService::class);
        $scoringService->shouldReceive('getWeekResults')->once()->with(5)->andReturn(['results']);

        $controller = new LeagueController(
            Mockery::mock(GameSchedulerService::class),
            $scoringService,
            Mockery::mock(LeagueSimulationService::class)
        );

        $response = $controller->getWeekResults(5);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(['results'], $response->getData(true));
    }

    public function testGetWeekPredictions(): void
    {
        $request = new Request(['data']);
        $scoringService = Mockery::mock(ScoringService::class);
        $scoringService->shouldReceive('getWeekPredictions')->once()->with(['data'])->andReturn(['predictions']);

        $controller = new LeagueController(
            Mockery::mock(GameSchedulerService::class),
            $scoringService,
            Mockery::mock(LeagueSimulationService::class)
        );

        $response = $controller->getWeekPredictions($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(['predictions'], $response->getData(true));
    }

    public function testSimulateWeek(): void
    {
        Artisan::shouldReceive('call')->once()->with('app:simulate', ['week' => 1]);

        $controller = new LeagueController(
            Mockery::mock(GameSchedulerService::class),
            Mockery::mock(ScoringService::class),
            Mockery::mock(LeagueSimulationService::class)
        );

        $response = $controller->simulateWeek(1);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals('"success"', $response->getContent());
    }
}
