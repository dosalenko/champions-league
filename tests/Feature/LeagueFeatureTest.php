<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class LeagueFeatureTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test starting the league simulation.
     *
     * @return void
     */
    public function testStartSimulation()
    {
        $response = $this->getJson('/api/start-simulation');

        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson(['success']);
    }

    /**
     * Test fetching the tournament table.
     *
     * @return void
     */
    public function testGetTournamentTable()
    {
        $response = $this->getJson('/api/tournament-table');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                '*' => [
                    'team_id',
                    'team_name',
                    'totalPoints',
                    'numberOfHomeWins',
                    'numberOfHomeLosses',
                    'numberOfAwayWins',
                    'numberOfAwayLosses',
                    'numberOfDraws',
                    'numberOfGames',
                    'numberOfGoalsScored',
                    'numberOfGoalsConceded',
                    'goalDifference',
                ],
            ]);
    }

    /**
     * Test fetching the results for a specific week.
     *
     * @return void
     */
    public function testGetWeekResults()
    {
        $week = 1;
        $response = $this->postJson("/api/week-results/{$week}");

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'home_team_goals',
                    'away_team_goals',
                    'homeTeam' => [
                        'name',
                    ],
                    'awayTeam' => [
                        'name',
                    ],
                ],
            ]);
    }

    /**
     * Test fetching the predictions for the current week.
     *
     * @return void
     */
    public function testGetWeekPredictions()
    {
        $response = $this->postJson('/api/week-predictions/1');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                '*' => [
                    'team_name',
                    'chance',
                ],
            ]);
    }

    /**
     * Test simulating matches for a specific week.
     *
     * @return void
     */
    public function testSimulateWeek()
    {
        $week = 1;
        $response = $this->postJson("/api/simulate-week/{$week}");

        $response->assertStatus(Response::HTTP_OK)
            ->assertExactJson(['success']);
    }
}
