<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name='description' content=""/>
    <meta name="keywords" content=""/>
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    @vite(['resources/js/app.js'])
</head>

<body>
<div id="app"></div>
</body>
</html>
